"THE BEER-WARE LICENSE" (Revision 42)
=====================================

kgadek@gmail.com and m4jkel@gmail.com wrote this file. As long as you retain
this notice you can do whatever you want with this stuff. If we meet some day,
and you think this stuff is worth it, you can buy us a beer in return.

Konrad Gądek and Michał Konarski.
